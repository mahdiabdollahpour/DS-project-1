package PQ;

import Agancies.Order;

public class MaxHeapNode {
    Order order;

    /**
     * constructor gets an order and sets it to field order
     * @param order
     */
    public MaxHeapNode(Order order) {
        this.order = order;
    }

    /**
     * retruns a string to represent the order
     * @return
     */
    @Override
    public String toString() {
        return order.toString();
    }

}
