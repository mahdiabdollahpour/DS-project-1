package PQ;

import Agancies.Order;

import java.util.ArrayList;

/**
 * a maxheap or say priority queue is used to store the order the benefit is that in this way
 * access to the order with highest immediacy level is efficient
 */
public class MaxHeap {
    /**
     * we a useless node in the at 0 so our indexing starts with 1
     * and its easier to handle
     */
    public MaxHeap() {
        arr.add(new MaxHeapNode(new Order("ezafe", 0, "ezafe")));
    }

    ArrayList<MaxHeapNode> arr = new ArrayList<>();

    /**
     * add a order to maxheap this takes O(log n)
     * the node is added at the end
     * and the while loop bubbles it up if needed
     *
     * @param or
     */
    public void add(Order or) {
        //    System.out.println("wanna add");
        MaxHeapNode orNode = new MaxHeapNode(or);
        arr.add(orNode);
        //  System.out.println("index of :" + arr.lastIndexOf(orNode));
        int i = arr.size() - 1;
        while (i > 1 && arr.get(i).order.getImmediacyLevel() > arr.get(i / 2).order.getImmediacyLevel()) {
            //  System.out.println("i is :" + i);
            Order father = arr.get(i / 2).order;
            arr.get(i).order = father;
            arr.get(i / 2).order = or;
            i = i / 2;
        }
    }

    /**
     * removes the root and returns it
     * call to solveHipak makes it in O(log n)
     *
     * @return
     */
    public Order delMax() {
        if (arr.size() <= 1) return null;
        if (arr.size() == 2) {
            return arr.remove(1).order;
        }
        Order max = arr.get(1).order;
        MaxHeapNode last = arr.remove(arr.size() - 1);
        arr.get(1).order = last.order;
        solveHipak(1);
        return max;
    }

    /**
     * when left and right are heap but root is ruining it its called hipak
     * this method fixes it
     * it runs in O(h) h = height of the node i
     *
     * @param i
     */
    private void solveHipak(int i) {
        if (2 * i >= arr.size()) {
            return;
        }
        // i father
        // k  son
        int k = 2 * i;

        if (k + 1 < arr.size() && (arr.get(k + 1).order.getImmediacyLevel() > arr.get(k).order.getImmediacyLevel())) {
            k++;
        }
        if (arr.get(i).order.getImmediacyLevel() > arr.get(k).order.getImmediacyLevel()) {
            return;
        } else if (arr.get(i).order.getImmediacyLevel() == arr.get(k).order.getImmediacyLevel()) {
            if (arr.get(i).order.time > arr.get(k).order.time) {
                return;
            }
        }
        // swap
        Order son = arr.get(k).order;
        Order father = arr.get(i).order;
        arr.get(k).order = father;
        arr.get(i).order = son;
        solveHipak(k);
    }
}
