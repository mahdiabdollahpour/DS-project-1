package MyLL;

import Agancies.Agancy;

/**
 * Linked list node contains a data with type of Agency
 * and a pointer to the next element
 */
public class LLNode {
    public Agancy data = new Agancy();
    LLNode next;

}
