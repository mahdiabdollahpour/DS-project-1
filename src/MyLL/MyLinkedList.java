package MyLL;

import Agancies.Order;
import GeneraliezedList.GenListNode;

/**
 * simple linked list is used to store agencies because it makes adding easy and also deletion is easy on structure
 */
public class MyLinkedList {

    LLNode first = new LLNode();

    /**
     * adding an Agency to the list with name of name
     * this requires a search that runs in O(n) and adding is constant
     * so it would be O(n) overall
     *
     * @param name
     */
    public void addAgancy(String name) {
        LLNode thereIsBefore = search(name);
        if (thereIsBefore != null) {
            System.out.println("Agency named " + name + " already exists");
            return;
        }

        LLNode p = first.next;

        LLNode q = new LLNode();
        q.data.setName(name);
        q.next = p;
        first.next = q;
        System.out.println("agency with name of " + name + " was added");
    }

    /**
     * deletes a service from the agency named agancyName
     * a search through agencies is needed that takes O(m) m = number of agencies
     * then delete from agency is called that is in O(t) t = number of services of that agency
     * so it is O( m + t ) overall
     *
     * @param serviceName
     * @param agancyName
     */
    public void deleteServiceFromAgency(String serviceName, String agancyName) {
        LLNode ag = search(agancyName);
        if (ag == null) {
            System.out.println("no agency found with name of " + agancyName);
            return;
        }
        ag.data.deleteService(serviceName);
    }

    /**
     * iterates through all agencies that is in theta(n)
     */
    public void listAgencies() {
        System.out.println("listing agencies");
        LLNode p = first;
        while (p.next != null) {
            p = p.next;
            System.out.println(p.data.getName());
        }
        System.out.println("end of agencies");
    }

    /**
     * iterates through all agencies that is in O(n)
     * and returns the node object of that agency
     *
     * @param name
     * @return
     */
    public LLNode search(String name) {
        LLNode p = first;
        while (p.next != null) {
            p = p.next;
            if (p.data.getName().equals(name)) {
                return p;
            }
        }
        return null;
    }

    //time not handled yet

    /**
     * creates an order and adds it to priority queue
     * checking if the agency has this service is done by hasService method that runs in
     * O(m) m = number of services in that agency and their subservices
     * Order instasiation is O(1)
     * and adding to maxheap is in O(log n) n = number of max heap elements
     * ( done in addOrder method of class agency)
     * so it will be O( m + log n ) overall
     *
     * @param agancyName
     * @param serviceName
     * @param customername
     * @param immediacyLevel
     */
    public void addOrderToAgancy(String agancyName, String serviceName, String customername, int immediacyLevel) {
        LLNode p = search(agancyName);
        if (p == null) {
            System.out.println("no agency found with name of " + agancyName);
            return;
        }
        GenListNode hasServices = p.data.hasService(serviceName);
        if (hasServices == null) {
            System.out.println("the service " + serviceName + " does not exist or this agency does not include it");
            return;
        }
        Order order = new Order(hasServices.data.getServiceName(), immediacyLevel, customername);
        p.data.addOrder(order);
    }

    /**
     * this method lists and does the orders
     * by calling listAndDoOrders
     * list and do orders is in O(n * logn)
     * so this method is in O(n * log n)
     *
     * @param agancyName
     */
    public void listOrders(String agancyName) {
        //   System.out.println("found agency");
        LLNode p = search(agancyName);
        if (p == null) {
            System.out.println("no agency with name of " + agancyName + " was found");
            return;
        }
        p.data.listAndDoOrders();
    }
}
