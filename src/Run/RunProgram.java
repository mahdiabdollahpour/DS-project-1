package Run;

import Agancies.Agancy;
import GeneraliezedList.GenList;
import GeneraliezedList.MainService;
import MyLL.*;

import java.util.Scanner;

/**
 * this class is where the programm is started
 * main method instantiates gets a command and goes to the desired case and calls the appropriate methods
 * the character _ is used instead of space to seperate words in commands
 * immediacy level must be an integer : 1 or 2 or 3
 */
public class RunProgram {

    public static void main(String[] args) {

        MyLinkedList agencies = new MyLinkedList();
        GenList services = new GenList();
        Agancy.setGenListHandler(services);

        while (true) {
            Scanner in = new Scanner(System.in);
            String input = in.next();
            input = input.toLowerCase();
            String[] cmd = input.split("_");


            if (cmd[0].equals("list")) {
                switch (cmd[1]) {
                    case "agencies":
                        agencies.listAgencies();

                        break;
                    case "orders":
                        //System.out.println("in routea");
                        agencies.listOrders(cmd[2]);

                        break;
                    case "services":
                        if (2 < cmd.length && cmd[2].equals("from")) {
                            services.listSubs(cmd[3]);
                        } else {

                            services.listAllServices();
                        }
                        break;
                    default:
                        System.out.println("command not defined");
                        break;
                }
            } else if (cmd[0].equals("add")) {
                switch (cmd[1]) {
                    case "agency":
                        agencies.addAgancy(cmd[2]);
                        break;
                    case "service":
                        services.add(cmd[2]);
                        break;
                    case "subservice":

                        services.addSubservice(cmd[4], cmd[2]);
                        break;
                    case "offer":
                        //  System.out.printf(cmd[2]);
                        MainService gln = (MainService) services.search(cmd[2], false);
                        if (gln == null) {
                            System.out.println("no main service with name of " + cmd[2]);
                            break;
                        }
                        LLNode node = agencies.search(cmd[4]);
                        if (node == null) {
                            System.out.println("no agancy with name of " + cmd[4]);
                            break;
                        }
                        node.data.addServicesToAgancy(gln);
                        break;
                    default:
                        System.out.println("command not defined");
                        break;
                }
            } else if (cmd[0].equals("order")) {
                int imm = Integer.parseInt(cmd[7]);
                if (imm != 1 && imm != 2 && imm != 3) {
                    System.out.println("Immediacy level must be an integer : 1 or 2 or 3");
                    continue;
                }
                agencies.addOrderToAgancy(cmd[3], cmd[1], cmd[5], Integer.parseInt(cmd[7]));
            } else if (cmd[0].equals("delete")) {
                agencies.deleteServiceFromAgency(cmd[1], cmd[3]);
                System.gc();
            } else {
                System.out.println("command not defined");
            }

        }
    }

}
