package GeneraliezedList;

/**
 * this interface helps to restrict access when class genlist is passed to
 * agency
 */
public interface GenListHandler {

    void deleteSerivce(String serviceName);

    GenListNode searchWorkHorse(GenListNode p, String serviceName, boolean andSubs);
}
