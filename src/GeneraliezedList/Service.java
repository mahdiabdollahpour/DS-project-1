package GeneraliezedList;

import java.util.Scanner;

/**
 * class service is data part of GenListNode
 */
public class Service {

    public String getServiceName() {
        return serviceName;
    }

    String serviceName;
    String carModel;
    String description;
    String technicalDescription;
    int cost;

    /**
     * this method gets the details of service from user
     * obviously running in O(1)
     */
    public void getDetailsFromUser() {
        Scanner in = new Scanner(System.in);

        System.out.println("Enter Car Model");
        String input = in.next();
        carModel = input;

        System.out.println("Enter Description for Customers");
        input = in.next();
        description = input;

        System.out.println("Enter Technical Description for Agencies");
        input = in.next();
        technicalDescription = input;

        System.out.println("Enter Cost");
        cost = in.nextInt();
    }

}
