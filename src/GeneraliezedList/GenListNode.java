package GeneraliezedList;

public class GenListNode {

    public GenListNode next;
    public GenListNode dlink;
    public Service data = new Service();

    /**
     * this method prints the node and the subservices of this node
     * this would take O( m + 1 ) where m is number of subservices of this node
     */
    public void print() {
        System.out.println(data.serviceName);
        if (dlink != null) {
            System.out.println(data.serviceName + " has sub services :");
            GenListNode p = dlink;
            while (p != null) {
                p.print();
                p = p.next;
            }
            System.out.println("end of " + data.serviceName + " subservices");
        }

    }

    /**
     * this method is called when garbage collector tries to remove the object
     * I overrided this method to log the event
     */
    protected void finalize() {
        System.out.println(data.getServiceName() + " was deleted");
    }
}
