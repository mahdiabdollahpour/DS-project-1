package GeneraliezedList;

/**
 * This class is a generleized likedlist inorder to store services.
 * any service can have subservices and so it is best to use generalized linked list for this purpose.
 * and linked list structre make deletion easier
 */
public class GenList implements GenListHandler {

    public GenListNode first = new GenListNode();

    /*
        just a constructor
     */
    public GenList() {
        first.data.serviceName = "ezafe";
    }

    /**
     * this method lists all of services and the subservices this runs in O(n) where n is
     * number of all nodes (services and subservices
     */
    public void listAllServices() {
        System.out.println("listing all servises");
        listFrom(first.next);
        System.out.println("end of all services list");
    }

    /**
     * this method list all services and subservices after the argument
     * this runs in O(m) where m is number of subservices of gln
     *
     * @param gln
     */

    private void listFrom(GenListNode gln) {
        //   System.out.println();
        GenListNode p = gln;

        while (p != null) {
            p.print();
            p = p.next;
        }

    }

    /**
     * deletes the service with name of serviceName
     * this runs in O(n) because the code has to search through all nodes
     *
     * @param serviceName
     */
    public void deleteSerivce(String serviceName) {
        GenListNode p = first.next;
        GenListNode q = first;
        //  if (p == null) return;
        if (p == null) {
            return;
        } else if (p.data.serviceName.equals(serviceName)) {
            first.next = null;
        }

        while (p != null && !p.data.serviceName.equals(serviceName)) {
            q = p;
            p = p.next;

        }
        if (p == null) {
            return;
        }
        q.next = p.next;//delteing it


    }

    /**
     * this method list all subservices of the given argument
     * <p>
     * this one first search for th name give the calls the listFrom method
     * this runs in O(n)
     * because there is a search that takes O(n) and the print that is less than the search
     *
     * @param name
     */

    public void listSubs(String name) {
        GenListNode gln = search(name, true);
        if (gln == null) {
            System.out.println("no service found with name of " + name);
            return;
        }
//        if (gln.dlink == null) {
//            System.out.println("service " + name + " has no subservices");
//            return;
//        }
        System.out.println("lising subservices of " + name);
        listFrom(gln.dlink);
        System.out.printf("end of subservies of " + name);
    }

    /**
     * this method adds a MAIN service to the list
     * check for the name to be valid that is in O(n)
     * and the adding is O(1)
     * so would be O(n) overall
     * this method sets the name of the servies to be added
     * and gets the other details in getDetailsFromUser() method of class service
     *
     * @param serviceName
     */
    public void add(String serviceName) {
        GenListNode isthereBefore = search(serviceName, true);
        if (isthereBefore != null) {
            System.out.println("a service with the same name has been added before");
            System.out.println("service names must be distnict");
            return;
        }

        MainService gln = new MainService();
        gln.data.serviceName = serviceName;
        gln.data.getDetailsFromUser();
        gln.next = first.next;
        first.next = gln;

    }

    /**
     * searches for a service and returns it
     * this take in O(m) and m would be
     * - number of all services if andSubs is true
     * - number of main services if andSubs is false
     * this is the Driver function and the real work is done by the Work Horse
     *
     * @param serviceName
     * @param andSubs     determines whether to search MAIN services or all services
     * @return
     */

    public GenListNode search(String serviceName, boolean andSubs) {//driver
        if (first.next == null) {
            return null;
        } else {
            return searchWorkHorse(first.next, serviceName, andSubs);
        }
    }

    /**
     * this method is the work horse search function
     * iterates over all of nodes after p and call itself on the sublists
     * this takes O(m) where m is number of nodes after p and their sublists
     *
     * @param p
     * @param serviceName
     * @param andSubs
     * @return
     */

    public GenListNode searchWorkHorse(GenListNode p, String serviceName, boolean andSubs) {
        GenListNode gln = p;
        while (gln != null) {
            if (andSubs && gln.dlink != null) {
                GenListNode samp = searchWorkHorse(gln.dlink, serviceName, andSubs);
                if (samp != null) {
                    return samp;

                }
            }
            if (gln.data.serviceName.equals(serviceName)) {
                return gln;
            }
            gln = gln.next;
        }
        return null;
    }

    /**
     * this method gets two names and add subservice to the super service
     * this requires two searches that each takes O(n)
     * and th adding is O(1)
     * so it would be O(n) overall
     *
     * @param supername
     * @param subservice
     */

    public void addSubservice(String supername, String subservice) {
        GenListNode isthereBefore = search(subservice, true);
        if (isthereBefore != null) {//already exists
            System.out.println("a service with the same name has been added before");
            System.out.println("service names must be distnict");
            return;
        }

        GenListNode superService = search(supername, true);
        if (superService == null) {//no such super service
            System.out.println("no services found with name of " + supername);
            return;
        }

        GenListNode toAdd = new GenListNode();
        toAdd.data.serviceName = subservice;
        toAdd.data.getDetailsFromUser();

        GenListNode dl = superService.dlink;
        if (dl == null) {
            superService.dlink = toAdd;
        } else {
            toAdd.next = dl;
            superService.dlink = toAdd;

        }

    }
}