package GeneraliezedList;

public class MainService extends GenListNode {
    /**
     * main service has all properties of a GenlistNode
     * it also has an integer ref witch denotes number of agencies that have the service
     * this property is only for main services
     */
    public int ref = 0;

    public MainService() {
        super();
    }

}
