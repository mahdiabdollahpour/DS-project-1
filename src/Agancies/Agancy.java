package Agancies;

import GeneraliezedList.GenListHandler;
import GeneraliezedList.GenListNode;
import GeneraliezedList.MainService;
import PQ.MaxHeap;

import java.util.ArrayList;

public class Agancy {

    static GenListHandler glh;
    ArrayList<MainService> services = new ArrayList<>();
    String name;

    MaxHeap orders = new MaxHeap();

    /**
     * this method accepts a reference of genlist but in shape of GenlistHandler interface
     * to restrict acces to only the methods that are neede in this class
     * it is in O(1) obviously
     *
     * @param glh
     */
    public static void setGenListHandler(GenListHandler glh) {
        Agancy.glh = glh;
    }

    /**
     * this method adds a service to the agency by increamenting ref of that service and adding that to arraylist
     * it is in O(1) obviously
     *
     * @param gln
     */
    public void addServicesToAgancy(MainService gln) {
        gln.ref++;
        services.add(gln);
        System.out.println("servise " + gln.data.getServiceName() + " was added to agency " + name);
    }

    /**
     * this method determines if the agency has the service with name serviceName or not
     * if yes returns the node object of the service
     * if not it returns null
     * this runs in O(m) where m is number of services in this agenct and their subservices
     *
     * @param serviceName
     * @return
     */
    public GenListNode hasService(String serviceName) {
        for (int i = 0; i < services.size(); i++) {
            if (services.get(i).data.getServiceName().equals(serviceName)) {
                return services.get(i);
            } else {
                GenListNode temp = glh.searchWorkHorse(services.get(i).dlink, serviceName, true);
                if (temp != null) {
                    return temp;
                }
            }
        }
        return null;
    }

    /**
     * deletes the service from this agency a search is needed over services that is O(m) m = number of services
     * if no other agency is pointing to that service the service is deleted
     * iterates throgh arraylist that takes O(m) where m is number of services in the arraylist , lenght of array list
     *
     * @param serviceName
     */
    public void deleteService(String serviceName) {
        MainService gln = null;
        for (int i = 0; i < services.size(); i++) {
            if (services.get(i).data.getServiceName().equals(serviceName)) {
                gln = services.get(i);
                break;
            }
        }
        if (gln == null) {
            System.out.println("no such service in this agency");
            return;
        }
        gln.ref--;
        if (gln.ref == 0) {
            glh.deleteSerivce(gln.data.getServiceName());
        }
        services.remove(gln);
    }

    /**
     * adding the order to the max heap that takes O( log n )
     *
     * @param o
     */
    public void addOrder(Order o) {
        orders.add(o);

    }

    /**
     * setter method
     * runs in o(1)
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * runs in O(1)
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * poping from max heap is in O( log n ) due to the fix we have to make after pop
     * and doing it while max haep is npt empty will be
     * O(n logn) overll
     */
    public void listAndDoOrders() {
        Order o = orders.delMax();
        // System.out.println("before loop");
        while (o != null) {
            System.out.println(o);
            //   System.out.println("in loop");
            o = orders.delMax();
        }
    }


}
