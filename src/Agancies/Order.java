package Agancies;

public class Order {

    String service;
    int immediacyLevel;
    String customerName;
    public long time;

//
//    public Order() {
//        time = System.currentTimeMillis();
//    }
//
//    public Order(int immediacyLevel) {
//
//        time = System.currentTimeMillis();
//        this.immediacyLevel = immediacyLevel;
//        serviceName = "sampleName";
//        customerName = "someName";
//    }

    @Override
    public String toString() {
        return "Order{ service= " + service  + '\'' +
                ", immediacyLevel=" + immediacyLevel +
                ", customerName='" + customerName + '\'' + " time :" + Long.toString(time) +
                '}';
    }

    public int getImmediacyLevel() {
        return immediacyLevel;
    }

    public Order(String service, int immediacyLevel, String customerName) {
        this.service = service;
        this.immediacyLevel = immediacyLevel;
        this.customerName = customerName;
        time = System.currentTimeMillis();
    }
}
